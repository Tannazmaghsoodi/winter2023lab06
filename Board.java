public class Board{
  
  private Die firstDie;
  private Die secondDie;
  private boolean[] tiles;
  
  //the constructor:
  public Board(){
     
    this.firstDie = new Die();
    this.secondDie = new Die();
    this.tiles = new boolean[12]; 
    
  }
  
  //override the toString method:
  public String toString(){
    
    String thePositionOfTheTiles=" ";
    
    for(int i=0 ; i < this.tiles.length ; i++){
      if(this.tiles[i] == false){
        
        thePositionOfTheTiles += i+1 + " ";
        
      } else {
        
        thePositionOfTheTiles+="X ";
        
      }
    } 
    return thePositionOfTheTiles;
  }
  
  //instance method that plays a turn 
  public boolean playATurn(){
   
    this.firstDie.roll();
    this.secondDie.roll();
    
    System.out.println("The first Die is: " + firstDie.toString() + " AND The second Die is: " + secondDie.toString());
    
    int sumOfDice = firstDie.getFaceValue() + secondDie.getFaceValue();
     
      if (this.tiles[firstDie.getFaceValue()-1]==false){
        
        this.tiles[sumOfDice-1] = true;
        System.out.println("Closing tile equal to sum: " + sumOfDice);
        return false;
        
      } else if(this.tiles[firstDie.getFaceValue()-1]==false){
        
        this.tiles[firstDie.getFaceValue()-1] = true;
        System.out.println("Closing tile with the same value as die one "+ firstDie.getFaceValue());
        return false;
        
      } else if(this.tiles[secondDie.getFaceValue()-1]==false){
         
        this.tiles[secondDie.getFaceValue()-1] = true;
        System.out.println("Closing tile with the same value as die one "+secondDie.getFaceValue());
        return false;
        
      } else {
        
        System.out.println("Closing tile with the same value as die one "+secondDie.getFaceValue());
        return false;
        
      }
  }
}
