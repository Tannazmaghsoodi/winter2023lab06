import java.util.Random;
public class Die{
 
  private int faceValue;
  private Random theRandom;
 
  //the constructor:
  public Die() {
    
    this.faceValue = 1;
    this.theRandom = new Random();
    
  }
  //the get method: 
  public int getFaceValue(){
    
    return this.faceValue;
    
  }
  //no set methods
  //roll method that sets the vallue of the faveValue fireld to a random number between 1 to 6 :
  public void roll(){
    
    this.faceValue = theRandom.nextInt(6)+1;
    
  }
  
  //addString methid that returns a String containing the value og the faceValue field and print the die object
  public String toString() {
    
    return Integer.toString(faceValue);
    
  }
}

