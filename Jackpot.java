public class Jackpot{
  public static void main(String[] args){
    
    System.out.println("Hi, welcome to jackpot program. Enjoy your game!");
    
    Board board = new Board();
    
    boolean gameOver = false; 
    
    int numOfTilesClosed = 0;
    
    while(!gameOver){
      
      System.out.println(board);
      
      if(board.playATurn()){
        
        gameOver=true;
        
      } else {
        
        numOfTilesClosed++;
        
      }
    }
    
    if (numOfTilesClosed >= 7) {
      
      System.out.println("You achieved JackPot!! YOU WON!!!! Congrats");
      
    } else {
      
      System.out.println("Unfortunately you couldn't make it this time. Play another round :(((");
      
    }
  }
}
